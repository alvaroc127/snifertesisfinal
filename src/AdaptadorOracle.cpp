#include "..\Include\AdaptadorOracle.h"



AdaptadorOracle::AdaptadorOracle() {


}

AdaptadorOracle::AdaptadorOracle(oracle::occi::ConnectionPool *conec) {
	this->conec = conec;

}

AdaptadorOracle::~AdaptadorOracle() {


}

void AdaptadorOracle::setConnectioPool(oracle::occi::ConnectionPool* conn) 
{
	this->conec = conn;
}

void AdaptadorOracle::insertar() 
{

}

void AdaptadorOracle::insertar(const std::string &param) 
{


}

void AdaptadorOracle::insertar(const std::string& ip, const int& ani, const int &mes, const int &dia, const int &hora, const int &min, const int &seg,const int& freac)
{
	oracle::occi::Connection* con = NULL;
	oracle::occi::Statement* stat = NULL;
	std::string nextSeq = "SELECT monseq.nextval from dual";
	//st.fromText(date, "yyyy-mm-dd hh:mi:ssxff", "",NULL);
	try {
		con=this->conec->createConnection(confUsPass::username,confUsPass::password);
		stat = con->createStatement(nextSeq);
		stat->executeQuery();
		stat ->setSQL("INSERT INTO MONITOR (ID,IP) VALUES (monseq.currval,:ip)");
		stat->setAutoCommit(true);
		stat->setString(1, ip);
		stat->executeUpdate();
	}
	catch (oracle::occi::SQLException exp) {
		std::cout << exp.getMessage() << std::endl;
	}
	catch (oracle::occi::BatchSQLException bat) {
		std::cout << bat.getMessage() << std::endl;
	}
	con->terminateStatement(stat);
	this->conec->terminateConnection(con);
}

void AdaptadorOracle::insertar(
	int alarmSeverity,
	std::string description,
	std::vector<uint8_t> ECG1,
	std::vector<uint8_t> ECG2,
	std::vector<uint8_t> ECG3,
	float aVR ,
	float aVl ,
	float frec_Card ,
	float I ,
	float II ,
	float III ,
	float V ,
	float aVF ,
	float CVP ,
	std::vector<std::string> mensajes,
	std::string insert,
	std::string update
) 
{
	oracle::occi::Connection* con=NULL;
	oracle::occi::Statement* stat=NULL;
	oracle::occi::Bytes ecg1(&ECG1[0], ECG1.size());
	oracle::occi::Bytes ecg2(&ECG2[0], ECG2.size());
	oracle::occi::Bytes ecg3(&ECG3[0], ECG3.size());
	std::string altersession = "ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'YYYY/MM/DD HH24:MI:SS.FF3'";
	try {
	con=this->conec->createConnection(confUsPass::username,confUsPass::password);
	stat=con->createStatement(altersession);
	stat->setAutoCommit(true);
	stat->execute();
	stat->setSQL(insert);
	stat->executeUpdate();
	stat->setSQL(update);
	stat->setBytes(1, ecg1);
	stat->setBytes(2, ecg2);
	stat->setBytes(3, ecg3);
	stat->setDouble(4, aVR);
	stat->setDouble(5, aVl);
	stat->setDouble(6, frec_Card);
	stat->setDouble(7, I);
	stat->setDouble(8, II);
	stat->setDouble(9, III);
	stat->setDouble(10, V);
	
	stat->setDouble(11, aVF);
	stat->setDouble(12, CVP);
	stat->executeUpdate();

	}
	catch (oracle::occi::SQLException exp) {
		std::cout << update << std::endl;
		std::cout << exp.getMessage();
	}
	con->terminateStatement(stat);
	this->conec->terminateConnection(con);
}


void AdaptadorOracle::insertar(const int &severidad,const std::string &descrip,const std::string &insert) {
	oracle::occi::Connection* con=NULL;
	oracle::occi::Statement* stat=NULL;
	std::string nextval = "SELECT alamSeq.nextval FROM dual";
	try {
		con = this->conec->createConnection(confUsPass::username,confUsPass::password);
		stat=con->createStatement(nextval);
		stat->setAutoCommit(true);
		stat->execute();
		stat->setSQL(insert);
		stat->executeUpdate();
	}
	catch (oracle::occi::SQLException exp){
		std::cout << exp.getMessage() << std::endl;
	}
	con->terminateStatement(stat);
	this->conec->terminateConnection(con);
}

void AdaptadorOracle::insertar(int id, std::string date_sig,
	float aVR, float aVL,
	float fre_Card, float I, float II,
	float III, float V,
	std::vector<uint8_t> ECG1, std::vector<uint8_t> ECG2, std::vector<uint8_t> ECG3,
	float aVF, float CVP, char bufferInset[], char bufferUpdate[])
{
	std::string SQLUPDATE =
		"update ECG SET  aVR = :aVR, aVL = :aVL, Frec_Cardi = :frecCard, I = :I, II = :II, III = :III, V = :V , ECG1 = :ECG1, ECG2  = :ECG2, ECG3 = :ECG3, aVF = :aVF, CVPs = :cVPs  WHERE ";
	std::string SQLInsert="";
	std::string alterSession= "ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'YYYY/MM/DD HH24:MI:SS.FF3'";
	SQLInsert=SQLInsert.append(bufferInset);
	SQLUPDATE=SQLUPDATE.append(bufferUpdate);
	oracle::occi::Bytes ecg1(&ECG1[0],ECG1.size());
	oracle::occi::Bytes ecg2(&ECG2[0], ECG2.size());
	oracle::occi::Bytes ecg3(&ECG3[0], ECG3.size());
	oracle::occi::Connection* conn=NULL;
	oracle::occi::Statement* stat=NULL;
	oracle::occi::ResultSet* rs=NULL;
	try {
		conn = this->conec->createConnection(confUsPass::username, confUsPass::password);
		stat = conn->createStatement(alterSession);
		stat->setAutoCommit(true);
		stat->execute();
		stat->setSQL(SQLInsert);
		stat->executeUpdate();
		stat->setSQL(SQLUPDATE);
		stat->setDouble(1, aVR);
		stat->setDouble(2, aVL);
		stat->setDouble(3, fre_Card);
		stat->setDouble(4, I);
		stat->setDouble(5, II);
		stat->setDouble(6, III);
		stat->setDouble(7, V);
		stat->setBytes(8, ecg1);
		stat->setBytes(9, ecg2);
		stat->setBytes(10, ecg3);
		stat->setDouble(11, aVF);
		stat->setDouble(12, CVP);
		stat->executeUpdate();
	}catch(oracle::occi::SQLException &exp) {
		std::cout << exp.getMessage() << std::endl;
	}
	conn->terminateStatement(stat);
	this->conec->terminateConnection(conn);
	}


void AdaptadorOracle::insertar(const float& impedancia, const  int& id, std::vector<uint8_t> sig, char bufferInsert[], char bufferUpdate[])
{
	oracle::occi::ResultSet* rs = NULL;
	oracle::occi::Statement* stat = NULL;
	oracle::occi::Connection* conn = NULL;
	oracle::occi::Bytes sigNal(&sig[0], sig.size());
	std::string SQLinsert = "";
	std::string alterSession = "ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'YYYY/MM/DD HH24:MI:SS.FF3'";
	SQLinsert=SQLinsert.append(bufferInsert);
	std::string SQLUPDATE = "UPDATE Frec_Respiratoria SET Impedancia = :inp , Senal = :sig WHERE ";
	SQLUPDATE=SQLUPDATE.append(bufferUpdate);
	try {
	conn = this->conec->createConnection(confUsPass::username,confUsPass::password);
	stat = conn->createStatement(alterSession);
	stat->setAutoCommit(true);
	stat->execute();
	stat->setSQL(SQLinsert);
	stat->executeUpdate();
	stat->setSQL(SQLUPDATE);
	stat->setFloat(1,impedancia);
	stat->setBytes(2,sigNal);
	stat->executeUpdate();
	}
	catch (oracle::occi::SQLException exp) {
		std::cout << exp.getMessage();
	}
	conn->terminateStatement(stat);
	this->conec->terminateConnection(conn);
}


void AdaptadorOracle::insertar(const float &max,const float &min,const float &paren,std::vector<uint8_t> sig, char bufferInsert[], char bufferUpdate[]) {
	
	oracle::occi::ResultSet* rs = NULL;
	oracle::occi::Statement* stat = NULL;
	oracle::occi::Connection* conn = NULL;
	oracle::occi::Bytes sigNal(&sig[0], sig.size());
	std::string SQLUPDATE = "UPDATE Senal_Roja_Amarilla SET  Maximo = :maximo , Minimo = :minio , Parentesis = :parentesis , Senal = :signal WHERE ";
	std::string SQLInsert = "";
	std::string alterSession = "ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'YYYY/MM/DD HH24:MI:SS.FF3'";
	SQLInsert=SQLInsert.append(bufferInsert);
	SQLUPDATE=SQLUPDATE.append(bufferUpdate);
	try {
		conn=this->conec->createConnection(confUsPass::username,confUsPass::password);
		stat = conn->createStatement(alterSession);
		stat->setAutoCommit(true);
		stat->execute();
		stat->setSQL(SQLInsert);
		stat->executeUpdate();
		stat->setSQL(SQLUPDATE);
		stat->setFloat(1,max);
		stat->setFloat(2,min);
		stat->setFloat(3,paren);
		stat->setBytes(4,sigNal);
		stat->executeUpdate();
	}
	catch (oracle::occi::SQLException exp) {
		std::cout << exp.getMessage();
	}
	conn->terminateStatement(stat);
	this->conec->terminateConnection(conn);
}

void AdaptadorOracle::consultar() {


}


void AdaptadorOracle::consultar(int &id, int&numcam, std::string&date_reg,  std::string& IP, bool &ban) 
{
	oracle::occi::ResultSet* rs=NULL;
	oracle::occi::Statement* stat = NULL;
	oracle::occi::Connection* conn = NULL;
	oracle::occi::Timestamp st;
	std::string copIP = IP;
	this->SQLconsulta=this->SQLconsulta + copIP + "'";
	try {
	conn = this->conec->createConnection(confUsPass::username,confUsPass::password);
	stat=conn->createStatement(this->SQLconsulta);
		rs=stat->executeQuery();
		while (rs->next()) {
			ban = true;
			id = rs->getInt(1);
			//IP.append(rs->getString(2));
		}
	}
	catch (oracle::occi::SQLException exp) {
		std::cout << exp.getMessage() << std::endl;
	}
	stat->closeResultSet(rs);
	conn->terminateStatement(stat);
	this->conec->terminateConnection(conn);
}

void AdaptadorOracle::actualizar() {

}



void AdaptadorOracle::setSQL(const std::string &sqlConsulta ) {
	this->SQLconsulta = sqlConsulta;
}

std::string AdaptadorOracle::getSqlConsulta() {
	return this->SQLconsulta;
}

void AdaptadorOracle::insertar(const float& frecuencia, const int& id, const float& desconocido, std::vector<uint8_t> signal, char bufferInsert[],char bufferUpdate[]) {
	oracle::occi::Connection* conn = NULL;
	oracle::occi::Statement* stat = NULL;
	std::string SQLInsert="";
	std::string SQLUPdate="UPDATE SPO2 SET  SenalSPO2 = :signal , Desconocido = :descon, frecuencia = :frecu WHERE ";
	std::string altersession = "ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'YYYY/MM/DD HH24:MI:SS.FF3'";
	oracle::occi::Bytes sigNal(&signal[0], signal.size());
	SQLInsert=SQLInsert.append(bufferInsert);
	SQLUPdate=SQLUPdate.append(bufferUpdate);
	try {
		conn = this->conec->createConnection(confUsPass::username, confUsPass::password);
		stat = conn->createStatement(altersession);
		stat->setAutoCommit(true);
		stat->execute();
		stat->setSQL(SQLInsert);
		stat->executeUpdate();
		stat->setSQL(SQLUPdate);
		stat->setBytes(1,sigNal);
		stat->setFloat(2,desconocido);
		stat->setFloat(3, frecuencia);
		stat->executeUpdate();
	}
	catch (oracle::occi::SQLException exp) {
		std::cout << exp.getMessage();
	}
	conn->terminateStatement(stat);
	this->conec->terminateConnection(conn);
}

void AdaptadorOracle::insertar(const float &T1,const float &T2,const float &T3,char bufferIns[],char bufferUpdate[]) {
	oracle::occi::Connection * con=NULL;
	oracle::occi::Statement * stat=NULL;
	std::string SQLInsert = "";
	std::string SQLUpdate = "UPDATE Temperatura SET T1 = :t1 , T2  = :t2, TD = :td WHERE ";
	std::string altersession = "ALTER SESSION SET NLS_TIMESTAMP_FORMAT = 'YYYY/MM/DD HH24:MI:SS.FF3'";
	SQLInsert=SQLInsert.append(bufferIns);
	SQLUpdate=SQLUpdate.append(bufferUpdate);
	try {
		con = this->conec->createConnection(confUsPass::username, confUsPass::password);
		stat = con->createStatement(altersession);
		stat->setAutoCommit(true);
		stat->execute();
		stat->setSQL(SQLInsert);
		stat->executeUpdate();
		stat->setSQL(SQLUpdate);
		stat->setFloat(1, T1);
		stat->setFloat(2, T2);
		stat->setFloat(3, T3);
		stat->executeUpdate();
	}
	catch (oracle::occi::SQLException *exp) {
		std::cout << exp->getMessage() <<std::endl;
	}
	con->terminateStatement(stat);
	this->conec->terminateConnection(con);
}

void AdaptadorOracle::consultar(int &id, std::string &descrip, int &severi, bool &bol) {
	oracle::occi::Connection *con = NULL;
	oracle::occi::Statement* stat = NULL;
	oracle::occi::ResultSet* re = NULL;
	try {
		con=this->conec->createConnection(confUsPass::username,confUsPass::password);
		stat = con->createStatement(SQLconsulta);
		re=stat->executeQuery();
		while (re->next()) {
			bol = true;
			id=re->getInt(1);
			severi = re->getInt(2);
		}
	}
	catch (oracle::occi::SQLException exp) {
		std::cout << exp.getMessage();
	}
	stat->closeResultSet(re);
	con->terminateStatement(stat);
	this->conec->terminateConnection(con);
}