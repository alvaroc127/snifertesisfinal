#include "../Include/OracConnect.h"

Oraconnect::Oraconnect() {

}

Oraconnect::~Oraconnect() {

}


void Oraconnect::OpenCo(){
	std::string connectString = "\/\/localhost:1521\/XE";
	uint8_t minConn = 1;
	uint8_t maxConn = 3;
	uint8_t incrConn = 2;
	try {
		
		this->env = oracle::occi::Environment::createEnvironment();
		this->conn = this->env->createConnectionPool(confUsPass::username, confUsPass::password, connectString, minConn, maxConn, incrConn);
	}
	catch (oracle::occi::SQLException &exp) {
		
		std::cout << exp.what() << std::endl;
	}
	this->open = true;
}

void Oraconnect::insertaDatTab() {
	this->mon->AdaptadorQueryMonitor();
	std::cout << "monitor insertado" << std::endl;
	loadOtherDat();
	this->ecg->loadECG(this->mon);
	this->ecg->setTimeStruc(this->st);
	this->ecg->insertAdaptador();

	this->Fres->setTimeStruc(this->st);
	this->Fres->loadFrecRes(mon);
	this->Fres->AdaptadorInsert();
	this->Fres->backEstad();

	this->spoBD->setTimeStruc(this->st);
	this->spoBD->loadSPO2(mon);
	this->spoBD->insertarAdaptador();
	this->spoBD->backEstad();

	this->sr->setTimeStruc(this->st);
	if (ga.sizeFile1(direcc + mon->getIp() + "\\ART.txt") > 0 || ga.sizeFile1(direcc + mon->getIp() + "\\ROJASign.bin") > 0) {
		sr->loadSArt(mon);
		this->sr->insertAdaptador();
	}
	if (ga.sizeFile1(direcc + mon->getIp() + "\\AP.txt") > 0 || ga.sizeFile1(direcc + mon->getIp() + "\\AMARILLASign.bin") > 0) {
		sr->loadSAP(mon);
		sr->insertAdaptador();
	}
	this->sr->backEstad();

	this->tbd->setTimeStruc(this->st);
	if (ga.sizeFile1(direcc + mon->getIp() + "\\TEMPPARAM.txt") > 0) {
		tbd->loadTemp(mon);
		this->tbd->insertAdaptador();
		this->tbd->backEstad();
	}
	abd->setTimeStruc(st);
	if (ga.sizeFile1(direcc + mon->getIp() + "\\ALARMAMEN.txt") > 0) {
		abd->loadMens(mon->getIp() + "\\ALARMAMEN.txt");
		for (int i = 0; i < abd->getMensajes().size(); i++) 
		{
			aidbd->setSeveri(abd->contChar(abd->getMensajes().at(i)));
			aidbd->setDescrip(abd->getMensajes().at(i));
			if (aidbd->queryAdaptadorAlarm() == false) 
			{
				aidbd->insertAdaptador();
				abd->loadAlarm(mon, ecg, aidbd);
				abd->insertAdaptador();
			}
			else {
				abd->loadAlarm(mon, ecg, aidbd);
				abd->insertAdaptador();
			}
		}
		abd->clearMensajes();
	}
	this->ecg->backEstad();
	}


void Oraconnect::loadOtherDat() {
	if (this->ecg==NULL) {
		this->ecg = new Ecg(this->ADAPTORORACLE,this->conn);
	}
	if (NULL==this->Fres) {
		this->Fres = new frec_respiratoriDB(this->ADAPTORORACLE, this->conn);
	}
	if (NULL == this->spoBD) {
		this->spoBD = new SPO2BD(this->ADAPTORORACLE,this->conn);
	}
	if (NULL == this->tbd) {
		this->tbd = new TemperaturaDB(this->ADAPTORORACLE,this->conn);
	}
	if (NULL == this->sr) {
		this->sr = new Senal_Roja_Amarilla(this->ADAPTORORACLE,this->conn);
	}
	if (NULL == this->abd) {
		this->abd = new AlarmaBD(this->ADAPTORORACLE,this->conn); 
	}
	if (NULL == aidbd) {
		this->aidbd = new AlarmaIDBD(this->ADAPTORORACLE,this->conn);
	}
}


void Oraconnect::setIp(const std::string& ipMon) {
	this->Ip = ipMon;
}



void Oraconnect::setDate(const std::string& datesys) {
	this->date = datesys;
}


void Oraconnect::loadDatTableMon() {
	if (mon == NULL) {
		mon = new Monitor1(this->ADAPTORORACLE,this->conn);
	}
	this->mon->setDate(this->date);
	this->mon->setTimeStruct(st);
	this->mon->setIp(this->Ip);
}


void Oraconnect::insertRegisMP(MindrayPacket) {


}


void  Oraconnect::insertRegisMA(MindrayAlarma) {

}


void Oraconnect::inserRegisMPP(MindrayParametros) {

}


bool Oraconnect::checkStore() {

	return false;
}


void Oraconnect::QueryMon() {

}



void Oraconnect::loadDate_Ip() {

}

void Oraconnect::show_Error(unsigned int, const SQLHANDLE&) {

}



void Oraconnect::Close() {
	this->env->terminateConnectionPool(this->conn);
}

void Oraconnect::terminateEnv() {
	oracle::occi::Environment::terminateEnvironment(this->env);
}

void Oraconnect::setStore(const Store&) {

}


void Oraconnect::insertSinAlarm() {

}


std::string Oraconnect::getFuente() {

	return "";
}


std::string Oraconnect::getDataTime() {

	return "";
}

bool Oraconnect::isOpen(){
return open;
}


void Oraconnect::setTimeStruc(const TIMESTAMP_STRUCT& st){
	this->st = st;
}



void Oraconnect::loadTable() {


}


void Oraconnect::setHeaders() {

}

bool Oraconnect::isConect2(){
	return true;
}
