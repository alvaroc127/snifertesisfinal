#if !defined (_INTERFACONNE_)
#define _INTERFACONNE_
#pragma once

#include <iostream>
#include <stdio.h>
#include <string>
#include "Ecg.h"
#include "AlarmaBD.h"
#include "frec_respiratoriDB.h"
#include "HabitacionDB.h"
#include "TemperaturaDB.h"
#include "Senal_Roja_Amarilla.h"
#include "Monitor1.h"
#include "StructDB.h"
#include "SubtRamTemp.h"
#include "SPO2BD.h"
#include "GestorArchivo.h"
#include "AlarmaIDBD.h"


class InterfaceConeccion
{
protected:
	Monitor1* mon = NULL;
	Ecg* ecg = NULL;
	frec_respiratoriDB* Fres = NULL;
	Senal_Roja_Amarilla* sr = NULL;
	HabitacionDB* hb = NULL;
	AlarmaBD* abd = NULL;
	SPO2BD* spoBD = NULL;
	TemperaturaDB* tbd = NULL;
	AlarmaIDBD* aidbd = NULL;
	Store al;
	GestorArchivo ga;

	std::string date;
	std::string Ip;
	bool open = false;
	bool open1 = false;

public:

	/// <summary>
	/// Finalizes an instance of the <see cref="Conection"/> class.
	/// </summary>
	virtual ~InterfaceConeccion();

	/// <summary>
	/// Checks the monitor.
	/// </summary>
	virtual void OpenCo()=0;

	/// <summary>
	/// Gets the conection.
	/// </summary>
	/// <returns></returns>
	virtual void insertaDatTab()=0;

	/// <summary>
	/// Loads the other dat.
	/// </summary>
	virtual void loadOtherDat()=0;

	/// <summary>
	/// Sets the ip.
	/// </summary>
	/// <param name="">The .</param>
	virtual void setIp(const std::string&)=0;


	/// <summary>
	/// Sets the date.
	/// </summary>
	/// <param name="">The .</param>
	virtual void setDate(const std::string&) =0;

	/// <summary>
	/// Loads the dat table mon.
	/// </summary>
	virtual void loadDatTableMon() =0;

	/// <summary>
	/// Inserts the regis mp.
	/// </summary>
	/// <param name="">The .</param>
	virtual void insertRegisMP(MindrayPacket)=0;

	/// <summary>
	/// Inserts the regis ma.
	/// </summary>
	/// <param name="">The .</param>
	virtual void insertRegisMA(MindrayAlarma)=0;

	/// <summary>
	/// Insers the regis MPP.
	/// </summary>
	/// <param name="">The .</param>
	virtual void inserRegisMPP(MindrayParametros)=0;

	/// <summary>
	/// Checks the store.
	/// </summary>
	/// <returns></returns>
	virtual bool checkStore()=0;


	/// <summary>
	/// Queries the mon.
	/// </summary>
	virtual void QueryMon()=0;


	/// <summary>
	/// Loads the date ip.
	/// </summary>
	virtual void loadDate_Ip()=0;

	/// <summary>
	/// Shows the error.
	/// </summary>
	virtual void show_Error(unsigned int, const SQLHANDLE&)=0;


	/// <summary>
	/// Closes this instance.
	/// </summary>
	virtual void Close()=0;

	/// <summary>
	/// Sets the store.
	/// </summary>
	/// <param name="">The .</param>
	virtual void setStore(const Store&)=0;

	/// <summary>
	/// Inserts the sin alarm.
	/// </summary>
	virtual void insertSinAlarm()=0;

	/// <summary>
	/// Gets the ip.
	/// </summary>
	/// <returns></returns>
	virtual std::string getFuente()=0;

	/// <summary>
	/// Gets the dATAtIME
	/// </summary>
	/// <returns></returns>
	virtual std::string getDataTime()=0;

	/// <summary>
	/// Determines whether this instance is open.
	/// </summary>
	/// <returns>
	///   <c>true</c> if this instance is open; otherwise, <c>false</c>.
	/// </returns>
	virtual bool isOpen()=0;

	/// <summary>
	/// Sets the time struc.
	/// </summary>
	/// <param name="st">The st.</param>
	virtual void setTimeStruc(const TIMESTAMP_STRUCT& st)=0;


	/// <summary>
	/// Loads the table.
	/// </summary>
	virtual void loadTable()=0;


	/// <summary>
	/// Sets the headers.
	/// </summary>
	virtual void setHeaders()=0;

	/// <summary>
	/// Determines whether this instance is conect2.
	/// </summary>
	/// <returns>
	///   <c>true</c> if this instance is conect2; otherwise, <c>false</c>.
	/// </returns>
	virtual bool isConect2()=0;


};
#endif
