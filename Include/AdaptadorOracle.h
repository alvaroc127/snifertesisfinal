#if !defined(_ADAPTERORACLE_)
#define _ADAPTERORACLE_
#pragma once
#include <Windows.h>
#include <string>
#include <occi.h>
#include "AdaptadorDB.h"




class AdaptadorOracle: public AdaptadorDB {
	
public:
	AdaptadorOracle();

	~AdaptadorOracle();

	AdaptadorOracle(oracle::occi::ConnectionPool *);

	void insertar();

	void insertar(const std::string &);

	void insertar(int id,
	std::string date_sig,
	float aVR,
	float aVL,
	float fre_Card,
	float I,
	float II,
	float III,
	float V,
	std::vector<uint8_t> ECG1,
	std::vector<uint8_t> ECG2,
	std::vector<uint8_t> ECG3,
	float aVF,
		float CVP,char bufferInset[],char bufferUpdate[]);

	void insertar(const float &,const  int &, std::vector<uint8_t> sig, char bufferInsert[],char bufferUpdate[]);

	void insertar(const float& max, const float& min, const float& paren, std::vector<uint8_t>, char bufferInsert[], char bufferUpdate[]);

	void insertar(const float&, const int&, const float&, std::vector<uint8_t>, char[],char []);

	void insertar(const float&,const float&,const float&,char [],char[]);

	void insertar(const int&, const std::string&, char[],char []);

	void insertar(const int&, const std::string&, const std::string&);

	void insertar(
	int alarmSeverity,
	std::string description,
	std::vector<uint8_t> ECG1,
	std::vector<uint8_t> ECG2,
	std::vector<uint8_t> ECG3,
	float aVR,
	float aVl,
	float frec_Card,
	float I,
	float II,
	float III,
	float V ,
	float aVF,
	float CVP,
	std::vector<std::string> mensajes,
		std::string insert,
		std::string update
	);

	void consultar();

	void consultar(int &,std::string &,int &, bool &);

	void consultar(int&,int&,std::string &,std::string &,bool &);

	void actualizar();

	void setSQL(const std::string &);

	void setConnectioPool(oracle::occi::ConnectionPool* conn);

	std::string getSqlConsulta();

	void insertar(const std::string&, const int &,const int& , const int &,const int &,const int &,const int&, const int &);

private:
	std::string SQLconsulta;
	oracle::occi::ConnectionPool * conec=NULL;
};


#endif