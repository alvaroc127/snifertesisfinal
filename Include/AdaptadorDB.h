#if!defined(_ADAPTADORDB_)
#define _ADAPTADORDB_
#pragma once
#include "confUsPass.h"

class AdaptadorDB {


public:
	AdaptadorDB();

	virtual ~AdaptadorDB();

	virtual void insertar() = 0;


	virtual void consultar() = 0;

	virtual void  actualizar() = 0;


};
#endif
