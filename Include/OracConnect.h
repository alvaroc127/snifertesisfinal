#if!defined(_CONEORCL_)
#define _CONEORCL_
#pragma once

#include <Windows.h>
#include <iostream>
#include <stdio.h>
#include <string>
#include <occi.h>
#include "Ecg.h"
#include "AlarmaBD.h"
#include "frec_respiratoriDB.h"
#include "HabitacionDB.h"
#include "TemperaturaDB.h"
#include "Senal_Roja_Amarilla.h"
#include "Monitor1.h"
#include "StructDB.h"
#include "SubtRamTemp.h"
#include "SPO2BD.h"
#include "GestorArchivo.h"
#include "AlarmaIDBD.h"
#include "InterfaceConeccion.h"
#include "confUsPass.h"



class Oraconnect: public InterfaceConeccion {
private:
	oracle::occi::Environment* env=NULL;
	oracle::occi::ConnectionPool* conn = NULL;
	char ADAPTORORACLE = 'O';
	int tipo;
	TIMESTAMP_STRUCT st;

	public:
		Oraconnect();

		~Oraconnect();

		/// <summary>
	/// Checks the monitor.
	/// </summary>
		void OpenCo();

		/// <summary>
		/// Gets the conection.
		/// </summary>
		/// <returns></returns>
		void insertaDatTab();

		/// <summary>
		/// Loads the other dat. is not functional in ORACLE
		/// </summary>
		void loadOtherDat();

		/// <summary>
		/// Sets the ip.
		/// </summary>
		/// <param name="">The .</param>
		void setIp(const std::string&);


		/// <summary>
		/// Sets the date.
		/// </summary>
		/// <param name="">The .</param>
		void setDate(const std::string&);

		/// <summary>
		/// Loads the dat table mon.
		/// </summary>
		void loadDatTableMon();

		/// <summary>
		/// Inserts the regis mp.
		/// </summary>
		/// <param name="">The .</param>
		void insertRegisMP(MindrayPacket);

		/// <summary>
		/// Inserts the regis ma.
		/// </summary>
		/// <param name="">The .</param>
		void insertRegisMA(MindrayAlarma);

		/// <summary>
		/// Insers the regis MPP.
		/// </summary>
		/// <param name="">The .</param>
		void inserRegisMPP(MindrayParametros);

		/// <summary>
		/// Checks the store.
		/// </summary>
		/// <returns></returns>
		bool checkStore();


		/// <summary>
		/// Queries the mon.
		/// </summary>
		void QueryMon();


		/// <summary>
		/// Loads the date ip.
		/// </summary>
		void loadDate_Ip();

		/// <summary>
		/// Shows the error.
		/// </summary>
		void show_Error(unsigned int, const SQLHANDLE&);


		/// <summary>
		/// Closes this instance.
		/// </summary>
		void Close();

		/// <summary>
		/// Sets the store.
		/// </summary>
		/// <param name="">The .</param>
		void setStore(const Store&);

		/// <summary>
		/// Inserts the sin alarm.
		/// </summary>
		void insertSinAlarm();

		/// <summary>
		/// Gets the ip.
		/// </summary>
		/// <returns></returns>
		std::string getFuente();

		/// <summary>
		/// Gets the dATAtIME
		/// </summary>
		/// <returns></returns>
		std::string getDataTime();

		/// <summary>
		/// Determines whether this instance is open.
		/// </summary>
		/// <returns>
		///   <c>true</c> if this instance is open; otherwise, <c>false</c>.
		/// </returns>
		bool isOpen();

		/// <summary>
		/// Sets the time struc.
		/// </summary>
		/// <param name="st">The st.</param>
		void setTimeStruc(const TIMESTAMP_STRUCT& st);


		/// <summary>
		/// Loads the table.
		/// </summary>
		void loadTable();


		/// <summary>
		/// Sets the headers.
		/// </summary>
		void setHeaders();

		/// <summary>
		/// Determines whether this instance is conect2.
		/// </summary>
		/// <returns>
		///   <c>true</c> if this instance is conect2; otherwise, <c>false</c>.
		/// </returns>
		bool isConect2();
		
		/// <summary>
		/// Terminates the env.
		/// </summary>
		void terminateEnv();
	

};

#endif
